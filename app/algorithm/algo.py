import math

def angle_to_meters(lon, lat):
    t = []
    # print("###", lon * 1000)
    t.append(lon * 111321.37 * math.cos(lat * math.pi / 180))
    # t.append(lon * 111321 * math.cos(lat * math.pi / 180))
    t.append(lat * 111134)  # metres
    return t

def meters_to_angle(x, y):
    t = []
    lat = x / 111134
    print("#", lat)
    # lon = y / 111321 / math.cos(round(lat, 10) * math.pi / 180)
    lon = y / 111321 / math.cos(lat * math.pi / 180)
    t.append(lat)
    t.append(lon)
    return t

def createDots(x1, y1, x3, y3, w, h):
    x2 = x3
    y2 = y1
    x4 = x1
    y4 = y1

    mas = []
    mx = w / 2
    my = h / 2
    H = abs(y1 - y3)
    W = abs(x1 - x3)
    isTurn = False

    while my < H - h:
        mx = w / 2
        while mx < W:
            mas.append((mx + x1, my + y1))
            mx+= w

        mx = W - w / 2
        mas.append((mx + x1, my + y1))
        my+= h
        mx+= w
        isTurn = not isTurn # to 1

        if my >= H:
            break
        mx-= w
        while mx > w / 2:
            mas.append((mx + x1, my + y1))
            mx-= w
        mx = w / 2
        mas.append((mx + x1, my + y1))
        my+= h
        isTurn = not isTurn # to 0

    my = H - h / 2
    if isTurn == False:
        mx = w / 2
        while mx < W:
            mas.append((mx + x1, my + y1))
            mx+= w

        mx = W - w / 2
        mas.append((mx + x1, my + y1))
    elif isTurn == True:
        mx = W - w / 2
        while mx > w / 2:
            mas.append((mx + x1, my + y1))
            mx-= w
        mx = w / 2
        mas.append((mx + x1, my + y1))
        my+= h



    return mas

# mas = createDots(0, 0, 50, 70, 20, 15)
# print(mas)

def dist(mas):
    res = 0
    for i in range(0, len(mas) - 1):
        # print("######", ((mas[i][0] * mas[i][0] - mas[i + 1][0] * mas[i + 1][0]) + (mas[i][1] * mas[i][1] - mas[i + 1][1] * mas[i + 1][1])))
        res+= math.sqrt((mas[i][0] - mas[i + 1][0]) * (mas[i][0] - mas[i + 1][0]) + (mas[i][1] - mas[i + 1][1]) * (mas[i][1] - mas[i + 1][1]))
    return res


def chooseHeight(x1, y1, x3, y3, d1, d2, bat, rash, alfa1, alfa2):
    # t = [
    #     [3500000, 5556700],
    #     [3506000, 5556700],
    #     [3506000, 5566700],
    #     [3500000, 5566700]
    # ]
    # print("###", angle_to_meters(50, 50))
    # t[0] = meters_to_angle(t[0][0], t[0][1])
    # t[1] = meters_to_angle(t[1][0], t[1][1])
    # t[2] = meters_to_angle(t[2][0], t[2][1])
    # t[3] = meters_to_angle(t[3][0], t[3][1])
    # return t


    # return [
    #     [50, 50], 
    #     [51, 50], 
    #     [51, 51], 
    #     [50, 51]
    # ]
    l = 0
    r = 1000
    eps = 0.001

    mas = None
    # x1, y1 = angle_to_meters(x1, y1)
    # x3, y3 = angle_to_meters(x3, y3)
    # d1, d2 = angle_to_meters(d1, d2)

    # y1, x1 = angle_to_meters(y1, x1)
    # y3, x3 = angle_to_meters(y3, x3)
    # d2, d1 = angle_to_meters(d2, d1)
    while r - l > eps:
        m = (r + l) / 2
        w = 2 * m * math.tan((alfa1 / 2) / 180 * math.pi)
        h = 2 * m * math.tan((alfa2 / 2) / 180 * math.pi)
        mas = createDots(x1, y1, x3, y3, w, h)
        print(mas)
        mas.insert(0, (d1, d2))
        #print(l, r, dist(mas) * rash, bat)
        if dist(mas) * rash > bat:
            l = m
        else:
            r = m
    # print(mas)
    # for i in range(0, len(mas)):
    #     # mas[i] = meters_to_angle(mas[i][0] + x1, mas[i][1] + y1)
    #     # continue
    #     if i > 0:
    #         mas[i] = meters_to_angle(mas[i][1] - h / 2, mas[i][0] + w / 2 - x1 + x3)
    #         mas[i][0], mas[i][1] = mas[i][1], mas[i][0]
    #     else:
    #         mas[i] = meters_to_angle(mas[i][1], mas[i][0])
    #         mas[i][0], mas[i][1] = mas[i][1], mas[i][0]
    return mas
