from flask import Flask
from config import Config

UPLOAD_FOLDER = '/Users/denisarkin/Desktop/TYI/Website/app/downloads'
ALLOWED_EXTENSIONS = set(['zip', 'jpg', 'JPG', 'png', 'PNG', 'JPEG'])

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DEBUG'] = True

app.config.from_object(Config)

from app import routes
