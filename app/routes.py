# -*- coding: utf-8 -*-
# import main_algo
# import vars 

import math
import sys
import os
import subprocess

from app import app
from app import ALLOWED_EXTENSIONS
from app.forms import LoginForm

from flask import render_template, request, redirect

from werkzeug.utils import secure_filename

# from .algorithm import main as algo
from .algorithm import vars, algo




# @app.route('/check')
# def check():
#     # return redirect('/index')
#     return render_template("check.html")


@app.after_request
def add_header(r):
    # """
    # Add headers to both force latest IE rendering engine or Chrome Frame,
    # and also to cache the rendered page for 10 minutes.
    # """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")


# @app.route('/algo')
# def algo_temp():
#     return render_template("index1.html")

@app.route('/wrongformat')
def wrongmap():
    return render_template("wrongformat.html")

@app.route('/showresult')
def showResult_temp():
    return render_template("showResult.html")

@app.route('/blur', methods=['GET', 'POST'])
def blur_temp():
    if request.method == 'POST':
        if 'file' not in request.files:
            print('No file part')
            return redirect('/wrongformat')
        file = request.files['file']
        if file.filename == '':
            # flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            subprocess.call('rm -rf app/downloads', shell=True)
            subprocess.call('mkdir app/downloads', shell=True)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], "photoset.zip"))

            subprocess.call("rm -rf app/static/res.jpg", shell=True)
            subprocess.call("mv app/downloads/photoset.zip BlurDetecting", shell=True)
            subprocess.call("cd BlurDetecting; python3 main.py", shell=True)
            subprocess.call("rm -rf BlurDetecting/photoset.zip", shell=True)
            subprocess.call("mv BlurDetecting/res.jpg app/static", shell=True)
            return redirect('/showresult')
    return render_template("index2.html")

@app.route('/map', methods=['GET', 'POST'])
def map_temp():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            subprocess.call('rm -rf app/downloads', shell=True)
            subprocess.call('mkdir app/downloads', shell=True)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], "photoset.zip"))

            subprocess.call("rm -rf app/static/res.jpg", shell=True)
            subprocess.call("mv app/downloads/photoset.zip CreatingMap", shell=True)
            subprocess.call("cd CreatingMap; python3 main.py", shell=True)
            subprocess.call("rm -rf CreatingMap/photoset.zip", shell=True)
            subprocess.call("mv CreatingMap/res.jpg app/static", shell=True)
            return redirect('/showresult')
    return render_template("index3.html")

@app.route('/water', methods=['GET', 'POST'])
def water_temp():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            subprocess.call('rm -rf app/downloads', shell=True)
            subprocess.call('mkdir app/downloads', shell=True)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], "photo.jpg"))
            subprocess.call("rm -rf app/static/res.jpg", shell=True)
            subprocess.call("mv app/downloads/photo.jpg FieldsDetecting", shell=True)
            subprocess.call("cd FieldsDetecting; python3 main.py", shell=True)
            subprocess.call("rm -rf FieldsDetecting/photo.jpg", shell=True)
            subprocess.call("mv FieldsDetecting/res.jpg app/static", shell=True)
            return redirect('/showresult')
    return render_template("index4.html")

@app.route('/algo', methods=['GET', 'POST'])
def buildroute():
    form = LoginForm(request.form)
    way = []
    if form.validate_on_submit():
        # way = [[50, 50], [40, 40], [50, 40]]
        # vars.target
        # way = algo.get_route()
        # print(form.data)
        data = form.data
        vars.target[0]['lon'] = float(data['first_dot_lon'])
        vars.target[0]['lat'] = float(data['first_dot_lat'])

        vars.target[1]['lon'] = float(data['first_dot_lon'])
        vars.target[1]['lat'] = float(data['second_dot_lat'])

        vars.target[2]['lon'] = float(data['second_dot_lon'])
        vars.target[2]['lat'] = float(data['second_dot_lat'])

        vars.target[3]['lon'] = float(data['second_dot_lon'])
        vars.target[3]['lat'] = float(data['first_dot_lat'])

        vars.drone['lon'] = float(data['drone_dot_lon'])
        vars.drone['lat'] = float(data['drone_dot_lat'])
        way = algo.chooseHeight(vars.target[0]['lon'], vars.target[0]['lat'], 
                                vars.target[2]['lon'], vars.target[2]['lat'],
                                vars.drone['lon'], vars.drone['lat'],
                                vars.battery_remaining, vars.battery_usage_per_meter, vars.A, vars.B
                                )
        print(way)
        return render_template("index1.html", form = form, info = way)
    return render_template("index1.html", form = form, info = way)

# @app.route('/login', methods = ['GET', 'POST'])
# def login():
#     form = LoginForm()
#     if form.validate_on_submit():
#         return redirect('/check')
#     return render_template("login.html", form = form)


# @app.route('/createmap', methods=['GET', 'POST'])
# def createMap():
#     if request.method == 'POST':
#       f = request.files['file']
#       f.save(secure_filename(f.filename))
#       return 'file uploaded successfully'
#     return render_template("createMap.html")
