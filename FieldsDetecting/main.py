import cv2
import numpy as np

## Read
img = cv2.imread("photo.jpg")

## convert to hsv
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

## mask of green (36,25,25) ~ (86, 255,255)
# mask = cv2.inRange(hsv, (36, 25, 25), (86, 255,255))
mask = cv2.inRange(hsv, (0, 75, 0), (32, 255,255))

## slice the red
imask = mask>0
red = np.zeros_like(img, np.uint8)
red[imask] = img[imask]


## slice the yellow
mask = cv2.inRange(hsv, (30.00001, 75, 0), (33, 255,255))

imask = mask>0
yel = np.zeros_like(img, np.uint8)
yel[imask] = img[imask]
#

## slice the blue
mask = cv2.inRange(hsv, (85, 0, 0), (130, 255,255))

imask = mask>0
blue = np.zeros_like(img, np.uint8)
blue[imask] = img[imask]
#

row, column, channels = img.shape

new_val = 210

for i in range(0, red.shape[0]):
    for j in range(0, red.shape[1]):
        if blue[i][j].all() != 0:
            img.itemset((i, j, 0), new_val)
        if yel[i][j].all() != 0:
            img.itemset((i, j, 1), new_val)
        if red[i][j].all() != 0:
            img.itemset((i, j, 2), new_val)
#cv2.imshow("img", img)

## save
cv2.imwrite("res.jpg", img)
# cv2.imshow("green.png", green)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
